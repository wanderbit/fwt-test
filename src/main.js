// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import vSelect from 'vue-select'
import Fragment from 'vue-fragment'
import addModal from './components/modal/add'
import editModal from './components/modal/edit'

Vue.use(Fragment.Plugin)
Vue.component('v-select', vSelect)
Vue.component('addModal', addModal)
Vue.component('editModal', editModal)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
