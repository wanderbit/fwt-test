import Vue from 'vue'
import Router from 'vue-router'

import Admin from '@/components/Admin'
import User from '@/components/User'
import Users from '@/components/Users'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'users',
      component: Users
    },
    {
      path: '/admin',
      name: 'admin',
      component: Admin
    },
    {
      path: '/user/:id',
      name: 'user',
      component: User,
      props: true
    }
  ]
})
