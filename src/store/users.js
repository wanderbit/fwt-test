class User {
  constructor (first, last, country, checked = false, id = null) {
    this.first = first
    this.last = last
    this.country = country
    this.checked = checked
    this.id = id
  }
}
const countries = ['Gomel', 'Minsk', 'Grodno']
export default {
  namespaced: true,
  state: {
    countries,
    users: [
      {
        first: 'First name',
        last: 'Last name',
        country: 'Minsk',
        checked: false,
        id: 0
      }
    ]
  },
  getters: {
    users (state) {
      return state.users
    },
    countries (state) {
      return state.countries
    },
    userById (state) {
      return userId => {
        return state.users.find(user => user.id === +userId)
      }
    }
  },
  mutations: {
    createUser (state, payload) {
      state.users.push(payload)
    },
    duplicateUser (state, id) {
      let obj = Object.assign({}, state.users[id])
      let lastEl = [...state.users].pop()
      let lastId = lastEl.id
      obj.id = lastId + 1
      state.users.push(obj)
    },
    deleteUser (state, id) {
      let idEl = id
      let index = state.users.map(function (item) {
        return item.id
      }).indexOf(idEl)
      state.users.splice(index, 1)
    },
    loadUsers (state, payload) {
      state.users = payload
    },
    changeCheck (state, payload) {
      const user = state.users.find(a => {
        return a.id === payload.id
      })
      user.checked = payload.value
    },
    deleteChecked (state) {
      state.users = state.users.filter(item => item.checked !== true)
    },
    updateUser (state, {first, last, country, id}) {
      const user = state.users.find(a => {
        return a.id === id
      })
      user.first = first
      user.last = last
      user.country = country
    }
  },
  actions: {
    async createUser ({commit, getters}, payload) {
      try {
        let id
        if (getters.users.length > 0) {
          const lastEl = [...getters.users].pop()
          id = lastEl.id
        } else {
          id = -1
        }
        const newUser = new User(
          payload.first,
          payload.last,
          payload.country,
          payload.checked,
          id + 1
        )
        commit('createUser', {
          ...newUser
        })
      } catch (error) {
        throw error
      }
    },
    async deleteUser ({commit}, payload) {
      try {
        commit('deleteUser', payload)
      } catch (error) {
        throw error
      }
    },
    async deleteChecked ({commit}) {
      try {
        commit('deleteChecked')
      } catch (error) {
        throw error
      }
    },
    async changeCheck ({commit}, payload) {
      try {
        commit('changeCheck', payload)
      } catch (error) {
        throw error
      }
    },
    async duplicateUser ({commit}, payload) {
      try {
        commit('duplicateUser', payload)
      } catch (error) {
        throw error
      }
    },
    async updateUser ({commit}, {first, last, country, id}) {
      try {
        commit('updateUser', {
          first, last, country, id
        })
      } catch (error) {
        throw error
      }
    }
  }
}
