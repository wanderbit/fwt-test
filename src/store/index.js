import Vue from 'vue'
import Vuex from 'vuex'
import modal from './modal'
import settings from './settings'
import users from './users'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex)
const vuexPersist = new VuexPersist({
  key: 'my-app',
  storage: localStorage
})
export default new Vuex.Store({
  modules: {
    modal,
    settings,
    users
  },
  plugins: [vuexPersist.plugin]
})
