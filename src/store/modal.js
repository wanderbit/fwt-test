export default {
  namespaced: true,
  state: {
    modalVisible: false,
    modalComponent: null,
    payload: null
  },
  mutations: {
    showModal (state, payload) {
      state.modalVisible = true
      state.modalComponent = payload.modal
      state.payload = payload.payload
    },
    hideModal (state) {
      state.modalVisible = false
    }
  },
  actions: {
    showModal ({commit}, payload) {
      commit('showModal', payload)
    }
  }
}
