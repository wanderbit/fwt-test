const fonts = ['Lato', 'Roboto', 'Inknut Antiqua', 'Mogra']
const colors = [
  {
    'hex': '#5EC4E2',
    'name': 'Viking'
  },
  {
    'hex': '#CD5C5C',
    'name': 'Indian Red'
  },
  {
    'hex': '#DC143C',
    'name': 'Crimson'
  },
  {
    'hex': '#20B2AA',
    'name': 'Light Sea Green'
  },
  {
    'hex': '#008B8B',
    'name': 'Dark Cyan'
  },
  {
    'hex': '#4682B4',
    'name': 'Steel Blue'
  }
]
export default {
  namespaced: true,
  state: {
    fonts,
    colors,
    selectedColorHex: '#5EC4E2',
    selectedFont: 'Lato',
    tableTitle: 'Users'
  },
  getters: {
    fonts (state) {
      return state.fonts
    },
    colors (state) {
      return state.colors
    },
    selectedColorHex (state) {
      return state.selectedColorHex
    },
    selectedFont (state) {
      return state.selectedFont
    },
    tableTitle (state) {
      return state.tableTitle
    }
  },
  mutations: {
    changeColors (state, color) {
      state.selectedColorHex = color
    },
    changeFont (state, font) {
      state.selectedFont = font
    },
    changeTitle (state, title) {
      state.tableTitle = title
    }
  },
  actions: {
    changeColors (store, color) {
      store.commit('changeColors', color)
    },
    changeFont (store, font) {
      store.commit('changeFont', font)
    },
    changeTitle (store, title) {
      store.commit('changeTitle', title)
    }
  }
}
